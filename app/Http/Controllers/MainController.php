<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller{
   
    public function index(){
        return view('index');
    }

    public function page2(){
        return view('page.page-2');

    }
  
}
