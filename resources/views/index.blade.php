<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Responsive - Test</title>
	<link rel="stylesheet" type="text/css" href="{!! asset('/css/main.css') !!}">
</head>
<body>
	<div class="grid-container">
    <div class="row">
       <div class="col-2 brand">
       <h1>YOUR LOGO</h1>
       </div>
       <div class="col-4 banner">
       		<h1>RESPONSIVE BANNER</h1>
       </div>
    </div> 
    <div class="row">
    	<div class="col-6 content">
    		<div class="read-more">
    			<a href="{!! url('/page-2'); !!}">READ MORE</a>
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-6 footer">
    	</div>
    </div>
</div>
</body>
</html>