<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Responsive - Test</title>
	<link rel="stylesheet" type="text/css" href="{!! asset('/css/main.css') !!}">
</head>
<body>
	<div class="grid-container">
    <div class="row">
       <div class="col-6 top-nav">
       		<div class="logo">
       			<p>LOGO</p>
       		</div>
       			<div class="blog">
       				<a href="{!! url('/'); !!}">Back</a>
       			</div>
       			<div class="topnav" id="myTopnav">
       				<a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
  					<a href="#news"><span>News</span></a>
  					<a href="#contact"><span>Contact</span></a>
  					<a href="#about"><span>About</span></a>
				</div>
       			<script type="text/javascript">
       				function myFunction() {
   						 var x = document.getElementById("myTopnav");
   						 if (x.className === "topnav") {
      					  x.className += " responsive";
   						 } else {
      					  x.className = "topnav";
    						}
						 }
       			</script>
       </div>
    </div> 
    <div class="row">
    	<div class="col-6 banner-full">
    	</div>
    </div>
    <div class="row">
    	<div class="col-6">
    		<div class="container">
    			<div class="item">
    				<div class="header-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    			</div>
    			<div class="item">
    				<div class="header-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    			</div>
    			<div class="item">
    				<div class="header-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    			</div>
    			<div class="item">
    				<div class="header-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    			</div>
    			<div class="item">
    				<div class="header-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    			</div>
    			<div class="item">
    				<div class="header-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    				<div class="title-item">
    				</div>
    			</div>
			</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-6 footer">
    	</div>
    </div>
</div>
</body>
</html>